import React from 'react';
import TelaCadastro from './cadastro';
import ReactDOM from 'react-dom';

ReactDOM.render(
    <React.StrictMode>
        <TelaCadastro />
    </React.StrictMode>,
    document.getElementById('root')
);