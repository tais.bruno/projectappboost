import React, { useState, useEffect, useCallback } from "react";
import axios from "axios";
import { cpf as cpfValidator } from "cpf-cnpj-validator";
import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css/dist/js/materialize.min.js';
import './index.css';

function TelaCadastro() {
    const [cpf, setCpf] = useState("");
    const [nome, setNome] = useState("");
    const [email, setEmail] = useState("");
    const [img1, setImg1] = useState("");
    const [img2, setImg2] = useState("");
    const [similarity, setSimilarity] = useState("");
    const [resultMessage, setResultMessage] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [formValid, setFormValid] = useState(false);
    const [savedId, setSavedId] = useState(null);

    useEffect(() => {
        M.AutoInit();
        var elems = document.querySelectorAll(".modal");
        M.Modal.init(elems);
    }, []);

    useEffect(() => {
        if (resultMessage !== "" && resultMessage !== "Face picture match") {
            openModal("Aviso", "As imagens fornecidas nao correspondem. Insira duas imagens do mesmo rosto.");
        }
    }, [resultMessage]);

    const openModal = useCallback((title, message) => {
        const utf8decoder = new TextDecoder("utf-8");
        const utf8message = utf8decoder.decode(new TextEncoder().encode(message));

        const modalContent = document.querySelector("#modal-content");
        modalContent.innerHTML = `<h5>${title}</h5><p>${utf8message}</p>`;
        setTimeout(() => {
            M.Modal.getInstance(document.querySelector("#modal1")).open();
        }, 100);
    }, []);

    const resetForm = useCallback(() => {
        setCpf("");
        setNome("");
        setEmail("");
        setImg1("");
        setImg2("");
        setSimilarity("");
        setResultMessage("");
    }, []);

    const isFormValid = useCallback(() => {
        return (
            cpf !== "" &&
            nome !== "" &&
            email !== "" &&
            img1 !== "" &&
            img2 !== "" &&
            similarity !== "" &&
            parseFloat(similarity) >= 80.00
        );
    }, [cpf, nome, email, img1, img2, similarity]);

    const validateCPF = useCallback((cpf) => {
        return cpfValidator.isValid(cpf);
    }, []);

    useEffect(() => {
        if (resultMessage === "Face picture match") {
            setFormValid(isFormValid());
        } else {
            setFormValid(false);
        }
    }, [resultMessage, isFormValid]);

    const handleCpfChange = useCallback(async (e) => {
        const cpf = e.target.value;
        setCpf(cpf);
    }, []);

    const handleBasicData = useCallback(async () => {
        if (cpf === "") {
            return;
        }
        if (validateCPF(cpf)) {
            try {
                setIsLoading(true);
                const response = await axios.post("https://localhost:7246/api/Cadastro/basic-data", { Cpf: cpf });
                const basicDataResponse = response.data;
                setNome(basicDataResponse);
                setFormValid(isFormValid());
            } catch (error) {
                console.log(error.response);
            } finally {
                setIsLoading(false);
                setFormValid(false);
            }
        } else {
            openModal("Aviso", "Insira um CPF valido");
        }
    }, [cpf, validateCPF, openModal, isFormValid]);

    const handleEmailValidation = useCallback(async (e) => {
        const email = e.target.value;
        if (email === "") {
            setFormValid(false);
            return;
        } else {
            try {
                setIsLoading(true);
                const response = await axios.post("https://localhost:7246/api/Cadastro/email-validation", { Email: email });
                const emailValidationResponse = response.data;
                console.log(emailValidationResponse)
                if (emailValidationResponse !== "VALID") {
                    openModal("Aviso", "Insira um email valido");
                    setFormValid(false);
                } else {
                    setFormValid(isFormValid());
                }
            } catch (error) {
                console.log(error.response);
            } finally {
                setIsLoading(false);
            }
        }
    }, [openModal, isFormValid]);

    const handleImagesChange = useCallback(async (e) => {
        if (e.target.files.length !== 2) {
            openModal("Aviso", "Selecione duas imagens");
            return;
        }
        const fileToUpload1 = e.target.files.item(0);
        const fileToUpload2 = e.target.files.item(1);

        const readAsDataURL = (file) => {
            return new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.onload = () => resolve(reader.result);
                reader.onerror = () => reject(reader.error);
                reader.readAsDataURL(file);
            });
        };
        try {
            const base64String1 = await readAsDataURL(fileToUpload1);
            const base64String2 = await readAsDataURL(fileToUpload2);
            setImg1(base64String1);
            setImg2(base64String2);
            setIsLoading(true);
            const facematchValidation = await axios.post("https://localhost:7246/api/Cadastro/user-facematch", { UserImg: base64String1.split(',')[1], UserImg2: base64String2.split(',')[1] });
            const responseData = JSON.parse(facematchValidation.data.content);
            const similarityValue = responseData.EstimatedInfo.Similarity;
            const resultMessage = responseData.ResultMessage;
            console.log(resultMessage);
            console.log(`Similarity: ${similarityValue}`);
            setSimilarity(similarityValue);
            setResultMessage(resultMessage);
            setFormValid(isFormValid());
        } catch (error) {
            console.log(`Error message: ${error.message}. Status: ${error.code}`);
        } finally {
            setIsLoading(false);
        }
    }, [openModal, isFormValid]);

    const sendDataApi = useCallback(async (e) => {
        e.preventDefault();
        if (similarity !== "" && parseFloat(similarity) >= 80.00 && resultMessage === "Face picture match") {
            try {
                setIsLoading(true);
                const response = await axios.post("https://localhost:7246/api/Pessoa/save", {
                    Cpf: cpf,
                    Nome: nome,
                    Email: email,
                    UserImg: img1.split(',')[1],
                    UserImg2: img2.split(',')[1],
                    Similarity: similarity,
                    ResultMessage: resultMessage
                });

                if (response.status === 200) {
                    setSavedId(response.data.id);
                    openModal("Dados salvos com sucesso!", `Seus dados foram salvos com sucesso. Obrigado por se cadastrar! ID do registro salvo: ${response.data.id}`);
                    resetForm();
                } else {
                    openModal("Aviso", "Ocorreu um erro ao salvar os dados do usuario");
                }
            } catch (error) {
                console.log(`Error message: ${error.message}. Status: ${error.code}`);
                openModal("Aviso", "Ocorreu um erro ao salvar os dados do usu�rio");
            } finally {
                setIsLoading(false);
            }
        } else {
            openModal("Aviso", "As imagens fornecidas n�o correspondem. Insira duas imagens da mesma pessoa.");
        }
    }, [similarity, resultMessage, cpf, nome, email, img1, img2, openModal, resetForm]);

    return (
        <form method='POST' onSubmit={sendDataApi}>
            {isLoading && (
                <div className="center-align" style={{ marginTop: '30px' }}>
                    <div className="preloader-wrapper small active">
                        <div className="spinner-layer spinner-green-only">
                            <div className="circle-clipper left">
                                <div className="circle"></div>
                            </div>
                            <div className="gap-patch">
                                <div className="circle"></div>
                            </div>
                            <div className="circle-clipper right">
                                <div className="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
            <br></br>
            <div>
                <label htmlFor="cpf">CPF:</label>
                <input
                    type="text"
                    id="cpf"
                    value={cpf}
                    onChange={handleCpfChange}
                    onBlur={handleBasicData}
                    required
                />
            </div>
            <br></br>
            <div>
                <label htmlFor="nome">Nome:</label>
                <input
                    type="text"
                    id="nome"
                    value={nome}
                    onChange={e => setNome(e.target.value)}
                    readOnly />
            </div>
            <br></br>
            <div>
                <label htmlFor="email">E-mail:</label>
                <input
                    type="email"
                    id="email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    onBlur={handleEmailValidation}
                    required
                />
            </div>
            <br></br>
            <br></br>
            <div>
                <label htmlFor="imagens">Insira um documento com foto e uma imagem do seu rosto para confirmar sua identidade:</label>
                <br></br>
                <label htmlFor="file-input" className="choose-files-btn">
                    Escolher Imagens
                </label>
                <input
                    type="file"
                    id="file-input"
                    name="file-input"
                    accept=".jpg,.jpeg,.png"
                    onChange={handleImagesChange}
                    multiple
                    required />
            </div>
            {similarity !== "" && parseFloat(similarity) >= 80.00 && resultMessage === "Face picture match" && (
                <p className="facematch-confere">Identidade confirmada!</p>
            )}
            {similarity !== "" && resultMessage !== "Face picture match" && (
                <p className="facematch-nao-confere">Identidade nao confirmada!</p>
            )}
            <br></br>
            <div id="modal1" className="modal">
                <div className="modal-content" id="modal-content"></div>
                <div className="modal-footer">
                    <a href="#!" className="modal-close waves-effect waves-green btn btn-flat">OK</a>
                </div>
            </div>
            <button
                className="btn waves-effect waves-light"
                type="submit"
                disabled={!formValid}
            >
                Enviar
            </button>
        </form>
    );

}

export default TelaCadastro;