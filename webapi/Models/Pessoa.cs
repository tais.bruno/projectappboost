﻿using MongoDB.Bson;
using System.ComponentModel.DataAnnotations;

namespace ProjectAppBoost.Models
{
    public class Pessoa
    {
        public ObjectId Id { get; set; }
        public string? Cpf { get; set; }
        public string? Nome { get; set; }
        [EmailAddress]
        public string? Email { get; set; }
        public string? UserImg { get; set; }
        public string? UserImg2 { get; set; }
        public string? Similarity { get; set; }
        public string? ResultMessage { get; set; }

    }
}