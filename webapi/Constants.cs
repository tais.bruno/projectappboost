﻿using static System.Net.WebRequestMethods;

namespace ProjectAppBoost
{
    public class Constants
    {
        public static readonly string BasicDataAddress = "https://bigboost.bigdatacorp.com.br/peoplev2";
        public static readonly string EmailValidationAddress = "https://bigboost.bigdatacorp.com.br/validations";
        public static readonly string FacematchAddress = "https://bigid.bigdatacorp.com.br/bigid/biometrias/facematch";
        public static readonly string BasicData = "basic_data";
        public static readonly string EmailValidation = "ondemand_email";
        public static readonly string AccessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6InRoYWRldS5iYXJyb3NAYmlnZGF0YWNvcnAuY29tLmJyIiwibmJmIjoxNjgwMTg4NTM4LCJleHAiOjE2ODEzOTgxMzgsImlhdCI6MTY4MDE4ODUzOCwiaXNzIjoiQmlnIERhdGEgQ29ycC4iLCJwcm9kdWN0cyI6WyJCSUdCT09TVCJdLCJkb21haW4iOiJCSUdEQVRBIn0.Xuc-tn3MqJ-4Ast3QU-BdRUUmazfVfCGwrZv03ouq6w";
        public static readonly string TokenID = "6425a47a89eb520008a5876f";
        public static readonly string MongoConnection = "mongodb+srv://taisbruno:K0fFtfZ3oGno59ZH@cluster0.bow8cjj.mongodb.net/?retryWrites=true&w=majority";
    }
}