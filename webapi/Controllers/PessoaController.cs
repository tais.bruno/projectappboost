﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using ProjectAppBoost;
using ProjectAppBoost.Models;
using System.Threading.Tasks;
using System;
using MongoDB.Bson;

namespace webapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PessoaController : ControllerBase
    {
        [HttpPost("save")]
        public async Task<IActionResult> SaveUserData([FromBody] Pessoa pessoa)
        {
            var client = new MongoClient(Constants.MongoConnection);
            var database = client.GetDatabase("BoostDatabase");
            var collection = database.GetCollection<Pessoa>("users");

            await collection.InsertOneAsync(pessoa);
            string savedId = pessoa.Id.ToString();
            return Ok(new { message = "Usuário salvo com sucesso!", id = savedId });
        }
    }
}


