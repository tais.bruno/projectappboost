﻿using Microsoft.AspNetCore.Mvc;
using ProjectAppBoost.Models;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using RestSharp;
using ProjectAppBoost;
using static System.Net.WebRequestMethods;
using System.Text.Json;
using System.Text;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;

namespace ProjectAppBoost.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CadastroController : ControllerBase
    {
        [HttpPost("basic-data")]
        public async Task<string> GetBasicData([FromBody] Pessoa pessoa)
        {
            var cpf = pessoa.Cpf;
            var cpfRequest = new { Datasets = Constants.BasicData, q = "doc{" + cpf + "}"};
            var json = JsonSerializer.Serialize(cpfRequest);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            string name;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("accept", "application/json");
                client.DefaultRequestHeaders.Add("Access-Control-Allow-Origin", "*");
                client.DefaultRequestHeaders.Add("AccessToken", Constants.AccessToken);
                client.DefaultRequestHeaders.Add("TokenId", Constants.TokenID);

                var responseJson = await client.PostAsync(Constants.BasicDataAddress, content);
                var responseContentjson = await responseJson.Content.ReadAsStringAsync();
                JsonDocument document = JsonDocument.Parse(responseContentjson);
                JsonElement root = document.RootElement;
                JsonElement nameElement = root.GetProperty("Result")[0].GetProperty("BasicData").GetProperty("Name");
                name = nameElement.GetString();
            }
            return name;
        }

        [HttpPost("email-validation")]
        public async Task<string> GetEmailValidation([FromBody] Pessoa pessoa)
        {
            var email = pessoa.Email;
            var emailRequest = new { Datasets = Constants.EmailValidation, q = "email{" + email + "}" };
            var json = JsonSerializer.Serialize(emailRequest);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            string validationStatus;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("accept", "application/json");
                client.DefaultRequestHeaders.Add("Access-Control-Allow-Origin", "*");
                client.DefaultRequestHeaders.Add("AccessToken", Constants.AccessToken);
                client.DefaultRequestHeaders.Add("TokenId", Constants.TokenID);

                var responseJson = await client.PostAsync(Constants.EmailValidationAddress, content);
                var responseContentjson = await responseJson.Content.ReadAsStringAsync();
                JObject jsonObject = JObject.Parse(responseContentjson);

                validationStatus = jsonObject["Result"][0]["EmailValidations"][0]["Status"].ToString();
                Console.WriteLine(validationStatus);
            }
            return validationStatus;
        }

        [HttpPost("user-facematch")]
        public RestResponse CheckFacematch([FromBody] Pessoa pessoa)
        {
            try
            {
                string UserImg = pessoa.UserImg;
                string UserImg2 = pessoa.UserImg2;
                var FacematchAPI = new RestClient(Constants.FacematchAddress);
                var postRequest = new RestRequest("", Method.Post)
                    .AddHeader("accept", "application/json")
                    .AddHeader("Access-Control-Allow-Origin", true)
                    .AddHeader("AccessToken", Constants.AccessToken)
                    .AddHeader("TokenId", Constants.TokenID)
                    .AddJsonBody(new { Parameters = new List<string> { $"BASE_FACE_IMG={UserImg}", $"MATCH_IMG={UserImg2}" } });

                RestResponse FacematchResponseData = FacematchAPI.Execute(postRequest);
                FacematchResponseData.StatusCode = HttpStatusCode.OK;

                return FacematchResponseData;
            }
            catch (ValidationException ex)
            {
                return new RestResponse { StatusCode = HttpStatusCode.BadRequest, ErrorMessage = "Validation Error: " + ex.Message };
            }
            catch (Exception ex)
            {
                return new RestResponse { StatusCode = HttpStatusCode.InternalServerError, ErrorMessage = "Internal Server Error: " + ex.Message };
            }
        }
    }
}